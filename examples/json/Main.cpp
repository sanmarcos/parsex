#include <fstream>
#include <iostream>

#include "parser.h"
#include "Visitor.h"

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cerr << "usage: " << argv[0] << " <grammar>" << std::endl;
        return 1;
    }
    std::ifstream f(argv[1]);
    Leksex l(f);
    try
    {
        ConfigureLekser(l);
        Exp* exp = Parse(l);
        Visitor v;
        exp->Accept(v);
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
