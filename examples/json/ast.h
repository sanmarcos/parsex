#pragma once

#include <memory>
#include <vector>
#include <string>

#include "Leksex.h"

enum Tokens
{
    Coma = 0,
    Colon = 1,
    OSq = 2,
    CSq = 3,
    False = 4,
    True = 5,
    OBrack = 6,
    CBrack = 7,
};

typedef std::string Identifier;
typedef StringLit StrLit;
typedef int Int;
class Visitor;
class Exp;
class ObjExp;
class ArrayExp;
class ValueExp;
class StrExp;
class IntExp;
class Field;
class Exp
{
private:
    const int uid_;

public:
    virtual
    void Accept(Visitor& v) const;
    virtual ~Exp() {}

    Exp();
};

class ObjExp : public Exp
{
private:
    const int uid_;
    std::vector<std::unique_ptr<Field>> fields_;

public:
    std::vector<std::unique_ptr<Field>>& fields();
    const std::vector<std::unique_ptr<Field>>& fields() const;
    void push_back(Field* fields);
    virtual
    void Accept(Visitor& v) const;
    virtual ~ObjExp() {}

    ObjExp();
};

class ArrayExp : public Exp
{
private:
    const int uid_;
    std::vector<std::unique_ptr<Exp>> vals_;

public:
    std::vector<std::unique_ptr<Exp>>& vals();
    const std::vector<std::unique_ptr<Exp>>& vals() const;
    void push_back(Exp* vals);
    virtual
    void Accept(Visitor& v) const;
    virtual ~ArrayExp() {}

    ArrayExp();
};

class ValueExp : public Exp
{
private:
    const int uid_;
    Identifier value_;

public:
    Identifier value() const;
    void value(Identifier value);
    virtual
    void Accept(Visitor& v) const;
    virtual ~ValueExp() {}

    ValueExp();
};

class StrExp : public Exp
{
private:
    const int uid_;
    StrLit value_;

public:
    StrLit value() const;
    void value(StrLit value);
    virtual
    void Accept(Visitor& v) const;
    virtual ~StrExp() {}

    StrExp();
};

class IntExp : public Exp
{
private:
    const int uid_;
    Int value_;

public:
    Int value() const;
    void value(Int value);
    virtual
    void Accept(Visitor& v) const;
    virtual ~IntExp() {}

    IntExp();
};

class Field
{
private:
    const int uid_;
    Identifier name_;
    std::unique_ptr<Exp> val_;

public:
    Identifier name() const;
    void name(Identifier name);
    Exp* val();
    const Exp* val() const;
    void val(Exp* val);
    void Accept(Visitor& v) const;
    Field();
};

