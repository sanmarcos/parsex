#pragma once

#include "Leksex.h"
#include "ast.h"

void ConfigureLekser(Leksex& lek);
Exp* Parse(Leksex& in);
Exp* ParseExp(Leksex& in);
ObjExp* ParseObjExp(Leksex& in);
ArrayExp* ParseArrayExp(Leksex& in);
ValueExp* ParseValueExp(Leksex& in);
StrExp* ParseStrExp(Leksex& in);
IntExp* ParseIntExp(Leksex& in);
Field* ParseField(Leksex& in);
