#include <iostream>

#include "Visitor.h"

Visitor::Visitor()
    : indent_(0)
{}

void Visitor::Visit(const Exp&)
{
}

void Visitor::Visit(const ObjExp& val)
{
    Indent();
    std::cout << "(ObjExp " << std::endl;
    ++indent_;
    for (auto& f : val.fields())
        f->Accept(*this);
    --indent_;
    Indent();
    std::cout << ")" << std::endl;
}

void Visitor::Visit(const ArrayExp& val)
{
    Indent();
    std::cout << "(ArrayExp " << std::endl;
    ++indent_;
    for (auto& v : val.vals())
        v->Accept(*this);
    --indent_;
    Indent();
    std::cout << ")" << std::endl;
}

void Visitor::Visit(const Field& val)
{
    Indent();
    std::cout << "(Field " << val.name() << std::endl;
    ++indent_;
    val.val()->Accept(*this);
    --indent_;
    Indent();
    std::cout << ")" << std::endl;
}

void Visitor::Visit(const ValueExp& val)
{
    Indent();
    std::cout << "(Id " << val.value() << ")" << std::endl;
}

void Visitor::Visit(const StrExp& val)
{
    Indent();
    std::cout << "(Str " << val.value() << ")" << std::endl;
}

void Visitor::Visit(const IntExp& val)
{
    Indent();
    std::cout << "(Int " << val.value() << ")" << std::endl;
}

void Visitor::Indent()
{
    for (int i = 0; i < indent_; ++i)
        std::cout << "  ";
}
