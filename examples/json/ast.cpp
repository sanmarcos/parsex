#include "ast.h"
#include "Visitor.h"

unsigned int GetUID()
{
    static unsigned int g_uid = 0;
    return g_uid++;
}

//===============================
// Exp
//===============================

void Exp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Exp::Exp()
    : uid_(GetUID())
{
}

//===============================
// ObjExp
//===============================

std::vector<std::unique_ptr<Field>>& ObjExp::fields() {
    return fields_;
}

const std::vector<std::unique_ptr<Field>>& ObjExp::fields() const {
    return fields_;
}

void ObjExp::push_back(Field* fields)
{
    fields_.push_back(std::unique_ptr<Field>(fields));
}

void ObjExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

ObjExp::ObjExp()
    : uid_(GetUID())
{
}

//===============================
// ArrayExp
//===============================

std::vector<std::unique_ptr<Exp>>& ArrayExp::vals() {
    return vals_;
}

const std::vector<std::unique_ptr<Exp>>& ArrayExp::vals() const {
    return vals_;
}

void ArrayExp::push_back(Exp* vals)
{
    vals_.push_back(std::unique_ptr<Exp>(vals));
}

void ArrayExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

ArrayExp::ArrayExp()
    : uid_(GetUID())
{
}

//===============================
// ValueExp
//===============================

Identifier ValueExp::value() const {
    return value_;
}

void ValueExp::value(Identifier value)
{
    value_ = value;
}

void ValueExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

ValueExp::ValueExp()
    : uid_(GetUID())
{
}

//===============================
// StrExp
//===============================

StrLit StrExp::value() const {
    return value_;
}

void StrExp::value(StrLit value)
{
    value_ = value;
}

void StrExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

StrExp::StrExp()
    : uid_(GetUID())
{
}

//===============================
// IntExp
//===============================

Int IntExp::value() const {
    return value_;
}

void IntExp::value(Int value)
{
    value_ = value;
}

void IntExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

IntExp::IntExp()
    : uid_(GetUID())
{
}

//===============================
// Field
//===============================

Identifier Field::name() const {
    return name_;
}

void Field::name(Identifier name)
{
    name_ = name;
}

Exp* Field::val() {
    return val_.get();
}

const Exp* Field::val() const {
    return val_.get();
}

void Field::val(Exp* val)
{
    val_ = std::unique_ptr<Exp>(val);
}

void Field::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Field::Field()
    : uid_(GetUID())
{
}

