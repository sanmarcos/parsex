#pragma once

#include "ast.h"

class Visitor
{
public:
    Visitor();
    virtual void Visit(const Exp& val);
    virtual void Visit(const ObjExp& val);
    virtual void Visit(const ArrayExp& val);
    virtual void Visit(const Field& val);
    virtual void Visit(const ValueExp& val);
    virtual void Visit(const StrExp& val);
    virtual void Visit(const IntExp& val);
private:
    int indent_;
    void Indent();
};
