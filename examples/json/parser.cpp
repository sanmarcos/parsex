#include <stdexcept>

#include "parser.h"

void ConfigureLekser(Leksex& lek)
{
    lek.AddSpecial(":", Tokens::Colon);
    lek.AddSpecial(",", Tokens::Coma);
    lek.AddSpecial("{", Tokens::OBrack);
    lek.AddSpecial("}", Tokens::CBrack);
    lek.AddSpecial("[", Tokens::OSq);
    lek.AddSpecial("]", Tokens::CSq);
    lek.AddSpecial("true", Tokens::True);
    lek.AddSpecial("false", Tokens::False);
}

Exp* Parse(Leksex& in)
{
    return ParseExp(in);
}

Exp* ParseExp(Leksex& in)
{
    if (in.Peek<Special>() == OBrack)
    {
        return ParseObjExp(in);
    }
    if (in.Peek<Special>() == OSq)
    {
        return ParseArrayExp(in);
    }
    if (in.Peek<std::string>())
    {
        return ParseValueExp(in);
    }
    if (in.Peek<StringLit>())
    {
        return ParseStrExp(in);
    }
    if (in.Peek<int>())
    {
        return ParseIntExp(in);
    }
    return nullptr;
}

ObjExp* ParseObjExp(Leksex& in)
{
    ObjExp* result = new ObjExp;

    if (in.GetToken<Special>() != OBrack)
        throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need {");
    if (in.Peek<std::string>())
    {
        result->push_back(ParseField(in));
        while (in.Peek<Special>() == Coma)
        {
            if (in.GetToken<Special>() != Coma)
                throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need ,");
            Field* iter = ParseField(in);
            result->push_back(iter);
        }
    }
    if (in.GetToken<Special>() != CBrack)
        throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need }");
    return result;
}

ArrayExp* ParseArrayExp(Leksex& in)
{
    ArrayExp* result = new ArrayExp;

    if (in.GetToken<Special>() != OSq)
        throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need [");
    if (in.Peek<std::string>() || in.Peek<StringLit>() || in.Peek<StringLit>() || in.Peek<Special>() == OSq || in.Peek<Special>() == OBrack)
    {
        result->push_back(ParseExp(in));
        while (in.Peek<Special>() == Coma)
        {
            if (in.GetToken<Special>() != Coma)
                throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need ,");
            Exp* iter = ParseExp(in);
            result->push_back(iter);
        }
    }
    if (in.GetToken<Special>() != CSq)
        throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need ]");
    return result;
}

ValueExp* ParseValueExp(Leksex& in)
{
    ValueExp* result = new ValueExp;

    result->value(*in.GetToken<std::string>());
    return result;
}

StrExp* ParseStrExp(Leksex& in)
{
    StrExp* result = new StrExp;

    result->value(*in.GetToken<StringLit>());
    return result;
}

IntExp* ParseIntExp(Leksex& in)
{
    IntExp* result = new IntExp;

    result->value(*in.GetToken<int>());
    return result;
}

Field* ParseField(Leksex& in)
{
    Field* result = new Field;

    result->name(*in.GetToken<std::string>());
    if (in.GetToken<Special>() != Colon)
        throw std::runtime_error(std::to_string(in.location().line) + ":"+ std::to_string(in.location().column) + ": unexpected token, need :");
    result->val(ParseExp(in));
    return result;
}

