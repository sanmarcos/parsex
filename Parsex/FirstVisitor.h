#pragma once

#include <set>
#include <string>

#include "Visitor.h"

#include "ast.h"

class FirstVisitor : public Visitor
{
public:
    virtual void Visit(const RuleWhile& alts) override;
    virtual void Visit(const RuleOption& alts) override;
    virtual void Visit(const Alternatives& alts) override;
    virtual void Visit(const Sequence& s) override;
    virtual void Visit(const NonTerminal& nt) override;
    virtual void Visit(const Terminal& nt) override;
    virtual void Visit(const NamedId& nt) override;

    FirstVisitor(const RuleGrammar& g);

    const std::set<std::string>& res() const;

private:
    const RuleGrammar& grammar_;
    std::set<std::string> res_;
    bool optional_;
};