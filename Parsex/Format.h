#pragma once

#include <string>
#include <sstream>

namespace
{
    template <class... Ts>
    void
    FormatImpl(std::ostringstream& oss, const char* begin, Ts... vals);

    template <class T, class... Ts>
    void
    FormatImpl(std::ostringstream& oss, const char* begin, T val,
                const Ts&... vals)
    {
        int percent = -1;
        for (int i = 0; begin[i]; ++i)
            if (begin[i] == '%')
            {
                percent = i;
                break;
            }

        if (percent != -1)
            oss.write(begin, percent);
        else
        {
            oss << begin;
            return;
        }
        oss << val;
        FormatImpl(oss, begin + percent + 1, vals...);
    }

#ifdef _MSC_VER
# pragma warning "Still necessary?"
    template <class... Ts>
#else
    template <>
#endif
    void
    FormatImpl(std::ostringstream& oss, const char* begin)
    {
        oss << begin;
    }
}

template <class... Ts>
std::string Format(const std::string& str, const Ts&... vals)
{
    std::ostringstream oss;
    FormatImpl(oss, &str[0], vals...);
    return oss.str();
}

