#include <iostream>

#include "Leksex.h"

#include "parser.h"
#include "ast.h"
#include "Type.h"
#include "ParserGenVisitor.h"
#include "TypeCollector.h"
#include "TypePrinter.h"
#include "Format.h"

int main(int argc, char** argv)
{
    TypeVisitor tv;

    try
    {
        if (argc < 2)
        {
            std::cerr << Format("usage: % <grammar_file>", argv[0])
                << std::endl;
            return 1;
        }
        std::ifstream file(argv[1]);
        Leksex l(file);
        ConfigureLekser(l);
        File* g = ParseFile(l);
        g->Accept(tv);
        auto& tys = tv.result();
        ParserGenVisitor pgv(tys);

        PrintTypes(tys);
        g->Accept(pgv);
        return 0;
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
