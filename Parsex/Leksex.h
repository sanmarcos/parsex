#pragma once

#include <iostream>
#include <cctype>
#include <string>
#include <memory>
#include <deque>
#include <map>

template <class T>
class Maybe
{
    T val_;
    bool full_;
public:
    Maybe(const T& val) : val_(val), full_(true) {}
    Maybe() : full_(false) {}

    operator bool() { return full_; }
    const T& operator * () { return  val_; }
    template <class U>
    bool operator==(const U& o) { return full_ && val_ == o; }
    template <class U>
    bool operator!=(const U& o) { return !full_ || val_ != o; }
};

class AbstractToken
{
public:
    AbstractToken(void) {}
    virtual ~AbstractToken(void) {}
    virtual void Pretty(std::ostream& in) = 0;
};

template <class T>
class Token : public AbstractToken
{
    T val_;

public:
    Token(const T& val) : val_(val) {}
    const T& value() { return val_; }
    virtual void Pretty(std::ostream& out)
    {
        out << "Token: " << val_ << std::endl;
    }
};

class Special
{
    int kw_;

public:
    Special(int id) : kw_(id) {}
    Special() : kw_(-1) {}
    operator int() { return kw_; }
};

class StringLit
{
    std::string lit_;
public:
    const std::string& lit() const { return lit_; }
    operator std::string () { return lit_; }
    operator const std::string& () { return lit_; }
    explicit StringLit(const std::string& s) : lit_(s) {}
    explicit StringLit() {}
};

template <class Strm>
Strm& operator<<(Strm& o, const StringLit& sl)
{
    return o << sl.lit();
}

typedef std::map<std::string, int> TokenTable;

class Leksex
{
public:
    struct Location
    {
        int line;
        int column;
        Location() : line(1), column(0) {}
    };

    Leksex(std::istream& in);

    template <class T>
    Maybe<T> GetToken();

    template <class T>
    Maybe<T> Peek(int lookahead = 0);

    AbstractToken* GetAbstractToken();

    inline Location location() const;
    inline void AddSpecial(const std::string& sym, int id);
    inline bool eof();
    inline void symbol_as_str();

    const TokenTable& specials() const;

private:
    inline bool IsDigit(char c);
    inline bool IsAlnum(char c);
    inline bool IsAlpha(char c);
    inline bool IsSpace(char c);
    inline bool IsSymbol(char c);
    inline char Peek();
    template <class T>
    Maybe<T> PopHelper();
    char GetChar();
    void EatSpaces();

    std::string DefaultIdentifierParser();
    std::string DefaultStringParser();
    int DefaultIntParser();
    int DefaultSymbolParser();

    std::istream& in_;
    Location loc_;
    bool dont_ignore_lf_;
    bool symbols_as_str_;
    TokenTable specials_;
    std::deque<std::unique_ptr<AbstractToken>> tokens_;
};

template <class T>
Maybe<T> Leksex::Peek(int la)
{
    int late = la - tokens_.size() + 1;
    while (late)
    {
        tokens_.emplace_back(std::unique_ptr<AbstractToken>(
            GetAbstractToken()));
        --late;
    }
    Token<T>* tok = dynamic_cast<Token<T>*>(tokens_[la].get());
    if (!tok)
        return Maybe<T>();
    return Maybe<T>(tok->value());
}

template <class T>
Maybe<T> Leksex::PopHelper()
{
    Token<T>* tok = dynamic_cast<Token<T>*>(tokens_.front().get());
    if (tok)
    {
        Maybe<T> t(tok->value());
        tokens_.pop_front();
        return t;
    }
    tokens_.pop_front();
    return Maybe<T>();
}

inline bool Leksex::eof()
{
    EatSpaces();
    return in_.eof();
}

inline bool Leksex::IsDigit(char c)
{
    return static_cast<int>(isdigit(c));
}

bool Leksex::IsAlnum(char c)
{
    return IsDigit(c) || isalpha(c);
}

bool Leksex::IsAlpha(char c)
{
    if (symbols_as_str_)
        return isalpha(c) || ispunct(c);
    return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}

bool Leksex::IsSymbol(char c)
{
    return specials_.find(std::string("") + c) != specials_.end();
}

void Leksex::AddSpecial(const std::string& sym, int id)
{
    specials_[sym] = id;
}

inline void Leksex::symbol_as_str()
{
    symbols_as_str_ = true;
}

inline Leksex::Location Leksex::location() const
{
    return loc_;
}