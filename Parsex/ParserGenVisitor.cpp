#include <iostream>
#include <stdexcept>

#include "FirstVisitor.h"
#include "ParserGenVisitor.h"
#include "Format.h"

ParserGenVisitor::ParserGenVisitor(const TypesDatabase& db)
    : db_(db)
#ifndef ON_STDOUT
    , hxx_("parser.h"), cxx_("parser.cpp")
#else
    , hxx_(std::cout), cxx_(std::cout)
#endif
{
}

void ParserGenVisitor::Visit(const File& g)
{
    indent_ = 1;
    hxx_ << "#pragma once" << std::endl
        << std::endl
        << "#include \"Leksex.h\"" << std::endl
        << "#include \"ast.h\"" << std::endl
        << std::endl
        << "void ConfigureLekser(Leksex& lek);" << std::endl;

    cxx_ << "#include <stdexcept>" << std::endl << std::endl
        << "#include \"parser.h\"" << std::endl
        << std::endl
        << "void ConfigureLekser(Leksex& lek)" << std::endl
        << "{" << std::endl;
    g.tokens()->Accept(*this);
    cxx_ << "}" << std::endl
        << std::endl;
    indent_ = 0;

    g.grammar()->Accept(*this);
}

void ParserGenVisitor::Visit(const TokConf& /*tc*/)
{
}

void ParserGenVisitor::Visit(const Symbol& sym)
{
    Indent() << Format("lek.AddSpecial(\"%\", Tokens::%);", sym.value().lit(),
                db_.symbols[sym.value().lit()])
        << std::endl;
}

void ParserGenVisitor::Visit(const RuleGrammar& g)
{
    grammar_ = &g;
    std::string first = g.rules()[0]->name();
    hxx_ << Format("%* Parse(Leksex& in);", first) << std::endl;
    cxx_ << Format("%* Parse(Leksex& in)", first) << std::endl
        << "{" << std::endl
        << Format("    return Parse%(in);", first) << std::endl
        << "}" << std::endl << std::endl;
    Visitor::Visit(g);
}

void ParserGenVisitor::Visit(const Rule& r)
{
    cur_rule_ = r.name();
    if (r.alts()->seqs().size() == 1)
    {
        FunctionPrologue(r.name());
        Visitor::Visit(r);
        FunctionEpilogue(r.name());
    }
    else
        Visitor::Visit(r);
}

void ParserGenVisitor::Visit(const Alternatives& alts)
{
    if (alts.seqs().size() == 1)
        Visitor::Visit(alts);
    else
    {
        hxx_ << Format("%* Parse%(Leksex& in);", cur_rule_, cur_rule_)
            << std::endl;
        cxx_ << Format("%* Parse%(Leksex& in)", cur_rule_, cur_rule_)
            << std::endl;
        cxx_ << "{" << std::endl;
        ++indent_;

        for (unsigned int i = 0; i < alts.seqs().size(); ++i)
        {
            Indent() << "if (";
            TestFirst(*alts.seqs()[i].get());
            cxx_ << ")" << std::endl;
            Indent() << "{" << std::endl;
            ++indent_;
            Indent() << "return Parse" << alts.seqs()[i]->type() << "(in);"
                << std::endl;
            --indent_;
            Indent() << "}" << std::endl;
        }

        Indent() << "return nullptr;" << std::endl;
        --indent_;
        cxx_ << "}" << std::endl << std::endl;

        Visitor::Visit(alts);
    }
}

void ParserGenVisitor::Visit(const Sequence& s)
{
    if (s.type() != "")
    {
        cur_rule_ = s.type();
        FunctionPrologue(s.type());
        Visitor::Visit(s);
        FunctionEpilogue(s.type());
    }
    else
    {
        Visitor::Visit(s);
    }
}

void ParserGenVisitor::Visit(const RuleExp& re)
{
    Visitor::Visit(re);
}

void ParserGenVisitor::Visit(const RuleWhile& rw)
{
    Indent() << "while (";
    TestFirst(*rw.exp());
    cxx_ << ")" << std::endl;
    Indent() << "{" << std::endl;
    ++indent_;
    Visitor::Visit(rw);
    last_tok_ = "";
    --indent_;
    Indent() << "}" << std::endl;
}

void ParserGenVisitor::Visit(const RuleOption& ro)
{
    Indent() << "if (";
    TestFirst(*ro.exp());
    cxx_ << ")" << std::endl;
    Indent() << "{" << std::endl;
    ++indent_;
    Visitor::Visit(ro);
    --indent_;
    Indent() << "}" << std::endl;
}

void ParserGenVisitor::Visit(const NonTerminal& nt)
{
    Indent();
    if (last_tok_ == nt.id())
    {
        // UGLY
        if (nt.id() == db_.id_ty)
            cxx_ << nt.id() << " iter = *in.GetToken<std::string>();" << std::endl;
        else if (nt.id() == db_.str_ty)
            cxx_ << nt.id() << " iter = *in.GetToken<std::string>();" << std::endl;
        else if (nt.id() == db_.int_ty)
            cxx_ << nt.id() << " iter = *in.GetToken<std::string>();" << std::endl;
        else
            cxx_ << nt.id() << "* iter = Parse" << nt.id() << "(in);" << std::endl;
    }


    Indent();
    if (last_tok_ == nt.id())
        cxx_ << "result->push_back(iter);" << std::endl;
}

void ParserGenVisitor::Visit(const Terminal& t)
{
    Indent() << "if (in.GetToken<Special>() != " << db_.symbols[t.id().lit()]
        << ")" << std::endl;
    // UGLY isolate in a function.
    Indent() << "    throw std::runtime_error(std::to_string(in.location().line) + \":\""
        "+ std::to_string(in.location().column) + \": unexpected token, need " << t.id()
        << "\");" << std::endl;
}

void ParserGenVisitor::Visit(const NamedId& ni)
{
    auto& ty = SearchType(cur_rule_).members();

    bool is_array = false;
    for (auto& m : ty)
    {
        if (m.name == ni.name())
        {
            is_array = m.is_array;
            break;
        }
    }

    Indent();
    // UGLY
    if (is_array)
    {
        if (ni.id() == db_.id_ty)
            cxx_ << "result->push_back(*in.GetToken<std::string>());"
                << std::endl;
        else if (ni.id() == db_.str_ty)
            cxx_ << "result->push_back(*in.GetToken<StringLit>());"
                << std::endl;
        else if (ni.id() == db_.int_ty)
            cxx_ << "result->push_back(*in.GetToken<int>());"
                << std::endl;
        else
            cxx_ << "result->push_back(Parse" << ni.id() << "(in));"
                << std::endl;
    }
    else
    {
        if (ni.id() == db_.id_ty)
            cxx_ << "result->" << ni.name() << "(*in.GetToken<std::string>());"
                << std::endl;
        else if (ni.id() == db_.str_ty)
            cxx_ << "result->" << ni.name() << "(*in.GetToken<StringLit>());"
                << std::endl;
        else if (ni.id() == db_.int_ty)
            cxx_ << "result->" << ni.name() << "(*in.GetToken<int>());"
                << std::endl;
        else
            cxx_ << "result->" << ni.name() << "(Parse" << ni.id() << "(in));"
                << std::endl;
    }
    last_tok_ = ni.id();
}

Type& ParserGenVisitor::SearchType(const std::string& ty_name)
{
    for (auto& ty : db_.types)
        if (ty.name() == ty_name)
            return ty;

    throw std::runtime_error("Can't find such a type " + ty_name +". "
        "You're running into big problems.");
}

void ParserGenVisitor::FunctionPrologue(const std::string& fun)
{
    hxx_ << Format("%* Parse%(Leksex& in);", fun, fun) << std::endl;

    Indent() << Format("%* Parse%(Leksex& in)", fun, fun) << std::endl;
    Indent() << "{" << std::endl;
    ++indent_;
    Type& ty = SearchType(fun);
    Indent() << ty.name() << "* result = new " << ty.name() << ";" << std::endl
        << std::endl;
}

void ParserGenVisitor::FunctionEpilogue(const std::string&)
{
        Indent() << "return result;" << std::endl;

        --indent_;
        cxx_ << "}\n\n";
}

std::ostream& ParserGenVisitor::Indent()
{
    for (int i = 0; i < indent_; ++i)
        cxx_ << "    ";
    return cxx_;
}
