#pragma once

#include <memory>
#include <vector>
#include <string>

#include "Leksex.h"

enum Tokens
{
    StarSym,
    MinusSym,
    ColonSym,
    DColonSym,
    SemicolonSym,
    LessSym,
    EqSym,
    GreaterSym,
    GrammarSym,
    SymbolsSym,
    TokensSym,
    OSquareSym,
    CSquareSym,
    OBraSym,
    PipeSym,
    CBraSym,
};

typedef std::string Identifier;
class Visitor;
class File;
class TokSection;
class TokConf;
class SymClause;
class Symbol;
class RuleGrammar;
class Rule;
class Alternatives;
class Sequence;
class RuleExp;
class RuleOption;
class RuleWhile;
class RuleId;
class Id;
class NonTerminal;
class Terminal;
class NamedId;
class File
{
private:
    const int uid_;
    std::unique_ptr<TokSection> tokens_;
    std::unique_ptr<RuleGrammar> grammar_;

public:
    TokSection* tokens();
    const TokSection* tokens() const;
    void tokens(TokSection* tokens);
    RuleGrammar* grammar();
    const RuleGrammar* grammar() const;
    void grammar(RuleGrammar* grammar);
    void Accept(Visitor& v) const;
    File();
};

class TokSection
{
private:
    const int uid_;
    std::vector<std::unique_ptr<TokConf>> general_conf_;
    std::unique_ptr<SymClause> symbols_;

public:
    std::vector<std::unique_ptr<TokConf>>& general_conf();
    const std::vector<std::unique_ptr<TokConf>>& general_conf() const;
    void push_back(TokConf* general_conf);
    SymClause* symbols();
    const SymClause* symbols() const;
    void symbols(SymClause* symbols);
    void Accept(Visitor& v) const;
    TokSection();
};

class TokConf
{
private:
    const int uid_;
    std::vector<Identifier> generalconf_;

public:
    const std::vector<Identifier>& generalconf() const;
    void push_back(Identifier generalconf);
    void Accept(Visitor& v) const;
    TokConf();
};

class SymClause
{
private:
    const int uid_;
    std::vector<std::unique_ptr<Symbol>> symbol_;

public:
    std::vector<std::unique_ptr<Symbol>>& symbol();
    const std::vector<std::unique_ptr<Symbol>>& symbol() const;
    void push_back(Symbol* symbol);
    void Accept(Visitor& v) const;
    SymClause();
};

class Symbol
{
private:
    const int uid_;
    StringLit value_;
    Identifier name_;

public:
    StringLit value() const;
    void value(StringLit value);
    Identifier name() const;
    void name(Identifier name);
    void Accept(Visitor& v) const;
    Symbol();
};

class RuleGrammar
{
private:
    const int uid_;
    std::vector<std::unique_ptr<Rule>> rules_;

public:
    std::vector<std::unique_ptr<Rule>>& rules();
    const std::vector<std::unique_ptr<Rule>>& rules() const;
    void push_back(Rule* rules);
    void Accept(Visitor& v) const;
    RuleGrammar();
};

class Rule
{
private:
    const int uid_;
    Identifier name_;
    std::unique_ptr<Alternatives> alts_;

public:
    Identifier name() const;
    void name(Identifier name);
    Alternatives* alts();
    const Alternatives* alts() const;
    void alts(Alternatives* alts);
    void Accept(Visitor& v) const;
    Rule();
};

class Alternatives
{
private:
    const int uid_;
    std::vector<std::unique_ptr<Sequence>> seqs_;

public:
    std::vector<std::unique_ptr<Sequence>>& seqs();
    const std::vector<std::unique_ptr<Sequence>>& seqs() const;
    void push_back(Sequence* seqs);
    void Accept(Visitor& v) const;
    Alternatives();
};

class Sequence
{
private:
    const int uid_;
    std::vector<std::unique_ptr<RuleExp>> exps_;
    Identifier type_;

public:
    std::vector<std::unique_ptr<RuleExp>>& exps();
    const std::vector<std::unique_ptr<RuleExp>>& exps() const;
    void push_back(RuleExp* exps);
    Identifier type() const;
    void type(Identifier type);
    void Accept(Visitor& v) const;
    Sequence();
};

class RuleExp
{
private:
    const int uid_;

public:
    virtual
    void Accept(Visitor& v) const;
    virtual ~RuleExp() {}

    RuleExp();
};

class RuleOption : public RuleExp
{
private:
    const int uid_;
    std::unique_ptr<Alternatives> exp_;

public:
    Alternatives* exp();
    const Alternatives* exp() const;
    void exp(Alternatives* exp);
    virtual
    void Accept(Visitor& v) const;
    virtual ~RuleOption() {}

    RuleOption();
};

class RuleWhile : public RuleExp
{
private:
    const int uid_;
    std::unique_ptr<Alternatives> exp_;

public:
    Alternatives* exp();
    const Alternatives* exp() const;
    void exp(Alternatives* exp);
    virtual
    void Accept(Visitor& v) const;
    virtual ~RuleWhile() {}

    RuleWhile();
};

class RuleId : public RuleExp
{
private:
    const int uid_;
    std::unique_ptr<Id> id_;

public:
    Id* id();
    const Id* id() const;
    void id(Id* id);
    virtual
    void Accept(Visitor& v) const;
    virtual ~RuleId() {}

    RuleId();
};

class Id
{
private:
    const int uid_;

public:
    virtual
    void Accept(Visitor& v) const;
    virtual ~Id() {}

    Id();
};

class NonTerminal : public Id
{
private:
    const int uid_;
    Identifier id_;

public:
    Identifier id() const;
    void id(Identifier id);
    virtual
    void Accept(Visitor& v) const;
    virtual ~NonTerminal() {}

    NonTerminal();
};

class Terminal : public Id
{
private:
    const int uid_;
    StringLit id_;

public:
    StringLit id() const;
    void id(StringLit id);
    virtual
    void Accept(Visitor& v) const;
    virtual ~Terminal() {}

    Terminal();
};

class NamedId : public Id
{
private:
    const int uid_;
    Identifier name_;
    Identifier id_;

public:
    Identifier name() const;
    void name(Identifier name);
    Identifier id() const;
    void id(Identifier id);
    virtual
    void Accept(Visitor& v) const;
    virtual ~NamedId() {}

    NamedId();
};

