#include <stdexcept>

#include "Leksex.h"


Leksex::Leksex(std::istream& in)
    : in_(in), dont_ignore_lf_(false), symbols_as_str_(false)
{
    if (!in_.good())
        throw std::runtime_error("File not properly opened");
}

int Leksex::DefaultIntParser()
{
    int i = 0;

    while (IsDigit(Peek()))
        i = i * 10 + GetChar() - '0';

    return i;
}

std::string Leksex::DefaultIdentifierParser()
{
    std::string id;

    if (symbols_as_str_ && ispunct(Peek()))
    {
        while (ispunct(Peek()))
            id += GetChar();
    }
    else
    {
        while (isalnum(Peek()) || Peek() == '_')
            id += GetChar();
    }
    return id;
}

char Leksex::Peek()
{
    return in_.peek();
}

char Leksex::GetChar()
{
    char c = in_.get();

    if (c == '\n')
    {
        loc_.column = 0;
        ++loc_.line;
    }
    else
        ++loc_.column;

    return c;
}

int Leksex::DefaultSymbolParser()
{
    std::string op;

    if (specials_.find(std::string("") + Peek()) != specials_.end())
    {
        op += GetChar();

        while (specials_.find(op + Peek()) != specials_.end())
            op += GetChar();

        return specials_[op];
    }
    return -1;
}

std::string Leksex::DefaultStringParser()
{
    std::string str;

    char enclosing = GetChar();

    while (!eof() && Peek() != enclosing)
    {
        char c = GetChar();
        if (c == '\\')
            continue;
        else
            str += c;
    }
    GetChar();
    return str;
}

bool Leksex::IsSpace(char c)
{
    if (dont_ignore_lf_)
        return c == ' ' || c == '\t';
    return c == ' ' || c == '\t' || c == '\n';
}

void Leksex::EatSpaces()
{
    while (IsSpace(Peek()))
        GetChar();
}

AbstractToken* Leksex::GetAbstractToken()
{
    EatSpaces();

    if (tokens_.size())
    {
        AbstractToken* tok  = tokens_.front().get();
        tokens_.front().release();
        tokens_.pop_front();
        return tok;
    }

    if (Peek() == '"' || Peek() == '\'')
        return new Token<StringLit>(StringLit(DefaultStringParser()));

    if (IsDigit(Peek()))
        return new Token<int>(DefaultIntParser());
    if (IsAlpha(Peek()))
    {
        std::string str = DefaultIdentifierParser();
        auto kw = specials_.find(str);
        if (kw == specials_.end())
            return new Token<std::string>(str);
        return new Token<Special>(kw->second);
    }
    if (IsSymbol(Peek()))
        return new Token<Special>(DefaultSymbolParser());
    return nullptr;
}

template <>
Maybe<int> Leksex::GetToken<int>()
{
    EatSpaces();

    if (tokens_.size())
        return PopHelper<int>();

    if (IsDigit(Peek()))
        return Maybe<int>(DefaultIntParser());
    return Maybe<int>();
}

template <>
Maybe<Special> Leksex::GetToken<Special>()
{
    EatSpaces();

    if (tokens_.size())
        return PopHelper<Special>();

    if (IsSymbol(Peek()))
        return Maybe<Special>(DefaultSymbolParser());
    if (IsAlpha(Peek()))
    {
        std::string str = DefaultIdentifierParser();
        auto kw = specials_.find(str);
        if (kw == specials_.end())
            return Maybe<Special>();
        return Maybe<Special>(kw->second);
    }
    return Maybe<Special>();
}

template <>
Maybe<std::string> Leksex::GetToken<std::string>()
{
    EatSpaces();

    if (tokens_.size())
        return PopHelper<std::string>();

    if (IsAlpha(Peek()))
    {
        std::string str = DefaultIdentifierParser();
        auto kw = specials_.find(str);
        if (kw == specials_.end())
            return Maybe<std::string>(str);
    }
    return Maybe<std::string>();
}

template <>
Maybe<StringLit> Leksex::GetToken<StringLit>()
{
    EatSpaces();

    if (tokens_.size())
        return PopHelper<StringLit>();

    if (Peek() == '"' || Peek() == '\'')
        return Maybe<StringLit>(StringLit(DefaultStringParser()));
    return Maybe<StringLit>();
}

const TokenTable& Leksex::specials() const
{
    return specials_;
}

