#pragma once

#include "Leksex.h"
#include "ast.h"

void ConfigureLekser(Leksex& lek);
File* ParseFile(Leksex& in);
TokSection* ParseTokSection(Leksex& in);
TokConf* ParseTokConf(Leksex& in);
SymClause* ParseSymClause(Leksex& in);
Symbol* ParseSymbol(Leksex& in);
RuleGrammar* ParseRuleGrammar(Leksex& in);
Rule* ParseRule(Leksex& in);
Alternatives* ParseAlternatives(Leksex& in);
Sequence* ParseSequence(Leksex& in);
RuleExp* ParseRuleExp(Leksex& in);
RuleOption* ParseRuleOption(Leksex& in);
RuleWhile* ParseRuleWhile(Leksex& in);
RuleId* ParseRuleId(Leksex& in);
Id* ParseId(Leksex& in);
NonTerminal* ParseNonTerminal(Leksex& in);
Terminal* ParseTerminal(Leksex& in);
NamedId* ParseNamedId(Leksex& in);
