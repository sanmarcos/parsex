#pragma once

#include <map>
#include <string>
#include <set>
#include <fstream>

#include "FirstVisitor.h"
#include "TypeCollector.h"

//#define ON_STDOUT

class ParserGenVisitor : public Visitor
{
public:
    virtual void Visit(const File& tc);
    virtual void Visit(const TokConf& tc);
    virtual void Visit(const Symbol& tc);
    virtual void Visit(const RuleGrammar& g);
    virtual void Visit(const Rule& r);
    virtual void Visit(const Alternatives& alts);
    virtual void Visit(const Sequence& s);
    virtual void Visit(const RuleExp& re);
    virtual void Visit(const RuleWhile& rw);
    virtual void Visit(const RuleOption& ro);
    virtual void Visit(const NonTerminal& nt);
    virtual void Visit(const Terminal& nt);
    virtual void Visit(const NamedId& nt);

    ParserGenVisitor(const TypesDatabase& db);

private:
    int indent_;
    std::ostream& Indent();
    void FunctionPrologue(const std::string& fun);
    void FunctionEpilogue(const std::string& fun);
    template <class Node>
    void TestFirst(const Node& n);
    Type& SearchType(const std::string& ty);
    std::string cur_rule_;
    std::string last_tok_;
    const RuleGrammar* grammar_;
    TypesDatabase db_;
#ifdef ON_STDOUT
    std::ostream& hxx_;
    std::ostream& cxx_;
#else
    std::ofstream hxx_;
    std::ofstream cxx_;
#endif
};

template <class Node>
void ParserGenVisitor::TestFirst(const Node& n)
{
    FirstVisitor fv(*grammar_);
    n.Accept(fv);
    auto first = fv.res();

    if (!first.empty())
    {
        auto f = first.begin();
        std::string tok_id = db_.symbols[*f];
        if (*f == db_.id_ty)
            cxx_ << "in.Peek<std::string>()";
        else if (*f == db_.str_ty)
            cxx_ << "in.Peek<StringLit>()";
        else if (*f == db_.int_ty)
            cxx_ << "in.Peek<int>()";
        else
            cxx_ << "in.Peek<Special>() == " << db_.symbols[*f];
        ++f;
        for (; f != first.end(); ++f)
        {
            tok_id = db_.symbols[*f];
            if (*f == db_.id_ty)
                cxx_ << " || in.Peek<std::string>()";
            else if (*f == db_.str_ty)
                cxx_ << " || in.Peek<StringLit>()";
            else if (*f == db_.int_ty)
                cxx_ << " || in.Peek<StringLit>()";
            else
                cxx_ << " || in.Peek<Special>() == " << db_.symbols[*f];
        }
    }
}

