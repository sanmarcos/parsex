#include "Type.h"

Type::Type(const std::string& ty)
    : ty_(ty), is_polymorphic_(false)
{
}

void Type::AddMember(const std::string& nm, const std::string& ty)
{
    Parameter p;
    p.type = ty;
    p.name = nm;
    members_.push_back(p);
}

void Type::is_polymorphic(bool poly)
{
    is_polymorphic_ = poly;
}

bool Type::is_polymorphic() const
{
    return is_polymorphic_;
}

void Type::base_class(const std::string& str)
{
    base_class_ = str;
    is_polymorphic_ = true;
}

std::string Type::base_class() const
{
    return base_class_;
}

const std::vector<Parameter>& Type::members() const
{
    return members_;
}

std::string Type::name() const
{
    return ty_;
}

void Type::LastAsArray()
{
    members_.back().is_array = true;
}

void Type::LastAsPrimitive()
{
    members_.back().is_primitive = true;
}