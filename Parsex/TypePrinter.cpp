#include <fstream>
#include <iostream>

#include "Format.h"
#include "TypePrinter.h"

//#define ON_STDOUT

void PrintTypesCxx(const TypesDatabase& infos)
{
    auto& types = infos.types;
#if defined ON_STDOUT
    std::ostream& cxx = std::cout;
#else
    std::ofstream cxx("ast.cpp");
#endif

    cxx << "#include \"ast.h\"" << std::endl
        << "#include \"Visitor.h\"" << std::endl
        << std::endl
        << "unsigned int GetUID()" << std::endl
        << "{" << std::endl << "    static unsigned int g_uid = 0;"
        << std::endl
        << "    return g_uid++;" << std::endl
        << "}" << std::endl << std::endl;

    for (auto& ty : types)
    {
        cxx << "//==============================="<< std::endl
            << "// " << ty.name() << std::endl
            << "//===============================" << std::endl << std::endl;

        for (auto& m : ty.members())
        {
            // Getter
            if (m.is_primitive && !m.is_array)
                cxx << Format("% %::%() const {",
                                m.type_ret(), ty.name(), m.name) << std::endl;
            else
                cxx << Format("% %::%() {", m.type_ret(), ty.name(), m.name)
                    << std::endl;
            if (m.is_array || m.is_primitive)
                cxx << Format("    return %_;", m .name) << std::endl;
            else
                cxx << Format("    return %_.get();", m.name) << std::endl;
            cxx << "}" << std::endl << std::endl;

            if (!m.is_primitive)
            {
                cxx << Format("const % %::%() const {",
                                m.type_ret(), ty.name(), m.name) << std::endl;
                if (m.is_array || m.is_primitive)
                    cxx << Format("    return %_;", m .name) << std::endl;
                else
                    cxx << Format("    return %_.get();", m.name) << std::endl;
                cxx << "}" << std::endl << std::endl;
            }

            // Setter
            if (m.is_array)
            {
                cxx << Format("void %::push_back(% %)",
                        ty.name(), m.type_param(), m.name) << std::endl
                    << "{" << std::endl
                    << Format("    %_.push_back(std::unique_ptr<%>(%));",
                                m.name, m.type, m.name) << std::endl
                    << "}" << std::endl << std::endl;
            }
            else
            {
                cxx << Format("void %::%(% %)", ty.name(), m.name,
                                m.type_param(), m.name) << std::endl
                    << "{" << std::endl;
                if (m.is_primitive)
                    cxx << Format("    %_ = %;", m.name, m.name) << std::endl;
                else
                    cxx << Format("    %_ = std::unique_ptr<%>(%);",
                                    m.name, m.type, m.name) << std::endl;
                cxx << "}" << std::endl << std::endl;
            }
        }

        // Visitor
        cxx << Format("void %::Accept(Visitor& v) const", ty.name()) << std::endl
            << "{" << std::endl
            << "    v.Visit(*this);" << std::endl
            << "}" << std::endl << std::endl;

        // Ctor
        cxx << Format("%::%()", ty.name(), ty.name()) << std::endl;

        // init_list
        cxx << "    : uid_(GetUID())";
        cxx << std::endl << "{" << std::endl << "}" << std::endl << std::endl;
    }
}

void PrintTypesHxx(const TypesDatabase& infos)
{
    auto& types = infos.types;
#if defined ON_STDOUT
    std::ostream& hxx = std::cout;
#else
    std::ofstream hxx("ast.h");
#endif
    hxx << "#pragma once" << std::endl
        << std::endl
        << "#include <memory>" << std::endl
        << "#include <vector>" << std::endl
        << "#include <string>" << std::endl
        << std::endl
        << "#include \"Leksex.h\"" << std::endl
        << std::endl;

    hxx << "enum Tokens" << std::endl
        << "{" << std::endl;

    {
        int cpt = 0;
        for (auto& sym : infos.symbols)
            hxx << Format("    % = %,", sym.second, cpt++) << std::endl;
    }

    hxx << "};" << std::endl << std::endl;

    hxx << "typedef std::string " << infos.id_ty << ";" << std::endl;
    hxx << "typedef StringLit " << infos.str_ty << ";" << std::endl;
    hxx << "typedef int " << infos.int_ty << ";" << std::endl;
    hxx << "class Visitor;" << std::endl;
    for (auto& ty : types)
        hxx << "class " << ty.name() << ";" << std::endl;

    for (auto& ty : types)
    {
        hxx << "class " << ty.name();

        if (ty.base_class() != "")
            hxx << " : public " << ty.base_class();

        hxx << std::endl << "{" << std::endl;
        hxx << "private:" << std::endl;

        hxx << "    const int uid_;" << std::endl;

        // fields
        for (auto& m : ty.members())
            hxx << Format("    % %_;", m.type_member(), m.name) << std::endl;

        hxx << std::endl << "public:" << std::endl;

        for (auto& m : ty.members())
        {
            // Getter
            if (m.is_primitive)
                hxx << Format("    % %() const;", m.type_ret(), m.name)
                    << std::endl;
            else
                hxx << Format("    % %();",  m.type_ret(), m.name)
                    << std::endl;

            if (!m.is_primitive)
                hxx << Format("    const % %() const;", m.type_ret(), m.name)
                    << std::endl;

            // Setter
            if (m.is_array)
                hxx << Format("    void push_back(% %);",
                                m.type_param(), m.name) << std::endl;
            else
                hxx << Format("    void %(% %);", m.name, m.type_param(),
                                m.name) << std::endl;
        }

        // Visitor
        if (ty.is_polymorphic())
            hxx << "    virtual" << std::endl;
        hxx << "    void Accept(Visitor& v) const;" << std::endl;

        // Dtor
        if (ty.is_polymorphic())
            hxx << Format("    virtual ~%() {}", ty.name()) << std::endl
                << std::endl;

        // Ctor
        hxx << Format("    %();", ty.name()) << std::endl;
        hxx << "};\n\n";
    }
}

void PrintTypes(const TypesDatabase& types)
{
    PrintTypesCxx(types);
    PrintTypesHxx(types);
}

