#pragma once

#include "Type.h"

#include "ast.h"
#include "Visitor.h"

struct TypesDatabase
{
    std::string int_ty;
    std::string id_ty;
    std::string str_ty;
    std::map<std::string, std::string> symbols;
    std::vector<Type> types;
};

class TypeVisitor : public Visitor
{
public:
    void Visit(const TokConf& r) override;
    void Visit(const SymClause& r) override;
    void Visit(const Rule& r) override;
    void Visit(const Alternatives& r) override;
    void Visit(const Sequence& s) override;
    void Visit(const NamedId& nid) override;
    void Visit(const Terminal& nid) override;
    void Visit(const NonTerminal& nid) override;
    void Visit(const RuleWhile& nid) override;
    TypeVisitor();

    const TypesDatabase& result() const;

private:
    TypesDatabase db_;
    bool last_was_member_;
};