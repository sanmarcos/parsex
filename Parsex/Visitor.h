
#pragma once

#include "ast.h"
#include "Type.h"

class Visitor
{
public:
    virtual void Visit(const File& ts);
    virtual void Visit(const TokSection& ts);
    virtual void Visit(const TokConf& ts);
    virtual void Visit(const SymClause& sc);
    virtual void Visit(const Symbol& s);
    virtual void Visit(const RuleGrammar& g);
    virtual void Visit(const Rule& r);
    virtual void Visit(const Alternatives& alts);
    virtual void Visit(const Sequence& s);
    virtual void Visit(const RuleExp& re);
    virtual void Visit(const RuleWhile& rw);
    virtual void Visit(const RuleOption& ro);
    virtual void Visit(const RuleId& ri);
    virtual void Visit(const Id& ri);
    virtual void Visit(const NonTerminal& nt);
    virtual void Visit(const Terminal& nt);
    virtual void Visit(const NamedId& nt);
};