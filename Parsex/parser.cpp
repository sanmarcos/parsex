#include <stdexcept>

#include "parser.h"

void ConfigureLekser(Leksex& lek)
{
    lek.AddSpecial("Tokens", Tokens::TokensSym);
    lek.AddSpecial("Symbols", Tokens::SymbolsSym);
    lek.AddSpecial("Grammar", Tokens::GrammarSym);
    lek.AddSpecial(":", Tokens::ColonSym);
    lek.AddSpecial("|", Tokens::PipeSym);
    lek.AddSpecial(";", Tokens::SemicolonSym);
    lek.AddSpecial("{", Tokens::OBraSym);
    lek.AddSpecial("}", Tokens::CBraSym);
    lek.AddSpecial("=", Tokens::EqSym);
    lek.AddSpecial("-", Tokens::MinusSym);
    lek.AddSpecial("[", Tokens::OSquareSym);
    lek.AddSpecial("]", Tokens::CSquareSym);
    lek.AddSpecial("<", Tokens::LessSym);
    lek.AddSpecial(">", Tokens::GreaterSym);
    lek.AddSpecial("::", Tokens::DColonSym);
    lek.AddSpecial("*", Tokens::StarSym);
}

File* ParseFile(Leksex& in)
{
    File* result = new File;

    if (in.GetToken<Special>() != TokensSym)
        throw std::runtime_error("unexpected token");
    if (in.GetToken<Special>() != EqSym)
        throw std::runtime_error("unexpected token");
    while (in.Peek<Special>() == EqSym)
    {
        if (in.GetToken<Special>() != EqSym)
            throw std::runtime_error("unexpected token");
    }
    result->tokens(ParseTokSection(in));
    if (in.GetToken<Special>() != GrammarSym)
        throw std::runtime_error("unexpected token");
    if (in.GetToken<Special>() != EqSym)
        throw std::runtime_error("unexpected token");
    while (in.Peek<Special>() == EqSym)
    {
        if (in.GetToken<Special>() != EqSym)
            throw std::runtime_error("unexpected token");
    }
    result->grammar(ParseRuleGrammar(in));
    return result;
}

TokSection* ParseTokSection(Leksex& in)
{
    TokSection* result = new TokSection;

    result->push_back(ParseTokConf(in));
    while (in.Peek<Special>() == StarSym)
    {
        TokConf* iter = ParseTokConf(in);
        result->push_back(iter);
    }
    result->symbols(ParseSymClause(in));
    return result;
}

TokConf* ParseTokConf(Leksex& in)
{
    TokConf* result = new TokConf;

    if (in.GetToken<Special>() != StarSym)
        throw std::runtime_error("unexpected token");
    result->push_back(*in.GetToken<std::string>());
    while (in.Peek<std::string>())
    {
        Identifier iter = *in.GetToken<std::string>();
        result->push_back(iter);
    }
    return result;
}

SymClause* ParseSymClause(Leksex& in)
{
    SymClause* result = new SymClause;

    if (in.GetToken<Special>() != SymbolsSym)
        throw std::runtime_error("unexpected token");
    if (in.GetToken<Special>() != MinusSym)
        throw std::runtime_error("unexpected token");
    while (in.Peek<Special>() == MinusSym)
    {
        if (in.GetToken<Special>() != MinusSym)
            throw std::runtime_error("unexpected token");
    }
    if (in.GetToken<Special>() != StarSym)
        throw std::runtime_error("unexpected token");
    result->push_back(ParseSymbol(in));
    while (in.Peek<Special>() == StarSym)
    {
        if (in.GetToken<Special>() != StarSym)
            throw std::runtime_error("unexpected token");
        Symbol* iter = ParseSymbol(in);
        result->push_back(iter);
    }
    return result;
}

Symbol* ParseSymbol(Leksex& in)
{
    Symbol* result = new Symbol;

    result->value(*in.GetToken<StringLit>());
    result->name(*in.GetToken<std::string>());
    return result;
}

RuleGrammar* ParseRuleGrammar(Leksex& in)
{
    RuleGrammar* result = new RuleGrammar;

    result->push_back(ParseRule(in));
    while (in.Peek<std::string>())
    {
        Rule* iter = ParseRule(in);
        result->push_back(iter);
    }
    return result;
}

Rule* ParseRule(Leksex& in)
{
    Rule* result = new Rule;

    result->name(*in.GetToken<std::string>());
    if (in.GetToken<Special>() != ColonSym)
        throw std::runtime_error("unexpected token");
    result->alts(ParseAlternatives(in));
    if (in.GetToken<Special>() != SemicolonSym)
        throw std::runtime_error("unexpected token");
    return result;
}

Alternatives* ParseAlternatives(Leksex& in)
{
    Alternatives* result = new Alternatives;

    result->push_back(ParseSequence(in));
    while (in.Peek<Special>() == PipeSym)
    {
        if (in.GetToken<Special>() != PipeSym)
            throw std::runtime_error("unexpected token");
        Sequence* iter = ParseSequence(in);
        result->push_back(iter);
    }
    return result;
}

Sequence* ParseSequence(Leksex& in)
{
    Sequence* result = new Sequence;

    while (in.Peek<Special>() == LessSym || in.Peek<std::string>() || in.Peek<StringLit>() || in.Peek<Special>() == OSquareSym || in.Peek<Special>() == OBraSym)
    {
        RuleExp* iter = ParseRuleExp(in);
        result->push_back(iter);
    }
    if (in.Peek<Special>() == DColonSym)
    {
        if (in.GetToken<Special>() != DColonSym)
            throw std::runtime_error("unexpected token");
        result->type(*in.GetToken<std::string>());
    }
    return result;
}

RuleExp* ParseRuleExp(Leksex& in)
{
    if (in.Peek<Special>() == OSquareSym)
    {
        return ParseRuleOption(in);
    }
    if (in.Peek<Special>() == OBraSym)
    {
        return ParseRuleWhile(in);
    }
    if (in.Peek<Special>() == LessSym || in.Peek<std::string>() || in.Peek<StringLit>())
    {
        return ParseRuleId(in);
    }
    return nullptr;
}

RuleOption* ParseRuleOption(Leksex& in)
{
    RuleOption* result = new RuleOption;

    if (in.GetToken<Special>() != OSquareSym)
        throw std::runtime_error("unexpected token");
    result->exp(ParseAlternatives(in));
    if (in.GetToken<Special>() != CSquareSym)
        throw std::runtime_error("unexpected token");
    return result;
}

RuleWhile* ParseRuleWhile(Leksex& in)
{
    RuleWhile* result = new RuleWhile;

    if (in.GetToken<Special>() != OBraSym)
        throw std::runtime_error("unexpected token");
    result->exp(ParseAlternatives(in));
    if (in.GetToken<Special>() != CBraSym)
        throw std::runtime_error("unexpected token");
    return result;
}

RuleId* ParseRuleId(Leksex& in)
{
    RuleId* result = new RuleId;

    result->id(ParseId(in));
    return result;
}

Id* ParseId(Leksex& in)
{
    if (in.Peek<std::string>())
    {
        return ParseNonTerminal(in);
    }
    if (in.Peek<StringLit>())
    {
        return ParseTerminal(in);
    }
    if (in.Peek<Special>() == LessSym)
    {
        return ParseNamedId(in);
    }
    return nullptr;
}

NonTerminal* ParseNonTerminal(Leksex& in)
{
    NonTerminal* result = new NonTerminal;

    result->id(*in.GetToken<std::string>());
    return result;
}

Terminal* ParseTerminal(Leksex& in)
{
    Terminal* result = new Terminal;

    result->id(*in.GetToken<StringLit>());
    return result;
}

NamedId* ParseNamedId(Leksex& in)
{
    NamedId* result = new NamedId;

    if (in.GetToken<Special>() != LessSym)
        throw std::runtime_error("unexpected token");
    result->name(*in.GetToken<std::string>());
    if (in.GetToken<Special>() != ColonSym)
        throw std::runtime_error("unexpected token");
    result->id(*in.GetToken<std::string>());
    if (in.GetToken<Special>() != GreaterSym)
        throw std::runtime_error("unexpected token");
    return result;
}

