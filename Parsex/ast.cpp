#include "ast.h"
#include "Visitor.h"

unsigned int GetUID()
{
    static unsigned int g_uid = 0;
    return g_uid++;
}

//===============================
// File
//===============================

TokSection* File::tokens() {
    return tokens_.get();
}

const TokSection* File::tokens() const {
    return tokens_.get();
}

void File::tokens(TokSection* tokens)
{
    tokens_ = std::unique_ptr<TokSection>(tokens);
}

RuleGrammar* File::grammar() {
    return grammar_.get();
}

const RuleGrammar* File::grammar() const {
    return grammar_.get();
}

void File::grammar(RuleGrammar* grammar)
{
    grammar_ = std::unique_ptr<RuleGrammar>(grammar);
}

void File::Accept(Visitor& v) const
{
    v.Visit(*this);
}

File::File()
    : uid_(GetUID())
{
}

//===============================
// TokSection
//===============================

std::vector<std::unique_ptr<TokConf>>& TokSection::general_conf() {
    return general_conf_;
}

const std::vector<std::unique_ptr<TokConf>>& TokSection::general_conf() const {
    return general_conf_;
}

void TokSection::push_back(TokConf* general_conf)
{
    general_conf_.push_back(std::unique_ptr<TokConf>(general_conf));
}

SymClause* TokSection::symbols() {
    return symbols_.get();
}

const SymClause* TokSection::symbols() const {
    return symbols_.get();
}

void TokSection::symbols(SymClause* symbols)
{
    symbols_ = std::unique_ptr<SymClause>(symbols);
}

void TokSection::Accept(Visitor& v) const
{
    v.Visit(*this);
}

TokSection::TokSection()
    : uid_(GetUID())
{
}

//===============================
// TokConf
//===============================

const std::vector<Identifier>& TokConf::generalconf() const {
    return generalconf_;
}

void TokConf::push_back(Identifier generalconf)
{
    generalconf_.push_back(generalconf);
}

void TokConf::Accept(Visitor& v) const
{
    v.Visit(*this);
}

TokConf::TokConf()
    : uid_(GetUID())
{
}

//===============================
// SymClause
//===============================

std::vector<std::unique_ptr<Symbol>>& SymClause::symbol() {
    return symbol_;
}

const std::vector<std::unique_ptr<Symbol>>& SymClause::symbol() const {
    return symbol_;
}

void SymClause::push_back(Symbol* symbol)
{
    symbol_.push_back(std::unique_ptr<Symbol>(symbol));
}

void SymClause::Accept(Visitor& v) const
{
    v.Visit(*this);
}

SymClause::SymClause()
    : uid_(GetUID())
{
}

//===============================
// Symbol
//===============================

StringLit Symbol::value() const {
    return value_;
}

void Symbol::value(StringLit value)
{
    value_ = value;
}

Identifier Symbol::name() const {
    return name_;
}

void Symbol::name(Identifier name)
{
    name_ = name;
}

void Symbol::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Symbol::Symbol()
    : uid_(GetUID())
{
}

//===============================
// RuleGrammar
//===============================

std::vector<std::unique_ptr<Rule>>& RuleGrammar::rules() {
    return rules_;
}

const std::vector<std::unique_ptr<Rule>>& RuleGrammar::rules() const {
    return rules_;
}

void RuleGrammar::push_back(Rule* rules)
{
    rules_.push_back(std::unique_ptr<Rule>(rules));
}

void RuleGrammar::Accept(Visitor& v) const
{
    v.Visit(*this);
}

RuleGrammar::RuleGrammar()
    : uid_(GetUID())
{
}

//===============================
// Rule
//===============================

Identifier Rule::name() const {
    return name_;
}

void Rule::name(Identifier name)
{
    name_ = name;
}

Alternatives* Rule::alts() {
    return alts_.get();
}

const Alternatives* Rule::alts() const {
    return alts_.get();
}

void Rule::alts(Alternatives* alts)
{
    alts_ = std::unique_ptr<Alternatives>(alts);
}

void Rule::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Rule::Rule()
    : uid_(GetUID())
{
}

//===============================
// Alternatives
//===============================

std::vector<std::unique_ptr<Sequence>>& Alternatives::seqs() {
    return seqs_;
}

const std::vector<std::unique_ptr<Sequence>>& Alternatives::seqs() const {
    return seqs_;
}

void Alternatives::push_back(Sequence* seqs)
{
    seqs_.push_back(std::unique_ptr<Sequence>(seqs));
}

void Alternatives::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Alternatives::Alternatives()
    : uid_(GetUID())
{
}

//===============================
// Sequence
//===============================

std::vector<std::unique_ptr<RuleExp>>& Sequence::exps() {
    return exps_;
}

const std::vector<std::unique_ptr<RuleExp>>& Sequence::exps() const {
    return exps_;
}

void Sequence::push_back(RuleExp* exps)
{
    exps_.push_back(std::unique_ptr<RuleExp>(exps));
}

Identifier Sequence::type() const {
    return type_;
}

void Sequence::type(Identifier type)
{
    type_ = type;
}

void Sequence::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Sequence::Sequence()
    : uid_(GetUID())
{
}

//===============================
// RuleExp
//===============================

void RuleExp::Accept(Visitor& v) const
{
    v.Visit(*this);
}

RuleExp::RuleExp()
    : uid_(GetUID())
{
}

//===============================
// RuleOption
//===============================

Alternatives* RuleOption::exp() {
    return exp_.get();
}

const Alternatives* RuleOption::exp() const {
    return exp_.get();
}

void RuleOption::exp(Alternatives* exp)
{
    exp_ = std::unique_ptr<Alternatives>(exp);
}

void RuleOption::Accept(Visitor& v) const
{
    v.Visit(*this);
}

RuleOption::RuleOption()
    : uid_(GetUID())
{
}

//===============================
// RuleWhile
//===============================

Alternatives* RuleWhile::exp() {
    return exp_.get();
}

const Alternatives* RuleWhile::exp() const {
    return exp_.get();
}

void RuleWhile::exp(Alternatives* exp)
{
    exp_ = std::unique_ptr<Alternatives>(exp);
}

void RuleWhile::Accept(Visitor& v) const
{
    v.Visit(*this);
}

RuleWhile::RuleWhile()
    : uid_(GetUID())
{
}

//===============================
// RuleId
//===============================

Id* RuleId::id() {
    return id_.get();
}

const Id* RuleId::id() const {
    return id_.get();
}

void RuleId::id(Id* id)
{
    id_ = std::unique_ptr<Id>(id);
}

void RuleId::Accept(Visitor& v) const
{
    v.Visit(*this);
}

RuleId::RuleId()
    : uid_(GetUID())
{
}

//===============================
// Id
//===============================

void Id::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Id::Id()
    : uid_(GetUID())
{
}

//===============================
// NonTerminal
//===============================

Identifier NonTerminal::id() const {
    return id_;
}

void NonTerminal::id(Identifier id)
{
    id_ = id;
}

void NonTerminal::Accept(Visitor& v) const
{
    v.Visit(*this);
}

NonTerminal::NonTerminal()
    : uid_(GetUID())
{
}

//===============================
// Terminal
//===============================

StringLit Terminal::id() const {
    return id_;
}

void Terminal::id(StringLit id)
{
    id_ = id;
}

void Terminal::Accept(Visitor& v) const
{
    v.Visit(*this);
}

Terminal::Terminal()
    : uid_(GetUID())
{
}

//===============================
// NamedId
//===============================

Identifier NamedId::name() const {
    return name_;
}

void NamedId::name(Identifier name)
{
    name_ = name;
}

Identifier NamedId::id() const {
    return id_;
}

void NamedId::id(Identifier id)
{
    id_ = id;
}

void NamedId::Accept(Visitor& v) const
{
    v.Visit(*this);
}

NamedId::NamedId()
    : uid_(GetUID())
{
}

