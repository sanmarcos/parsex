#include <iostream>
#include "TypeCollector.h"

#define ON_STDOUT

TypeVisitor::TypeVisitor()
{
}

void TypeVisitor::Visit(const TokConf& tc)
{
    auto confs = tc.generalconf();

    if (confs[0] == "default_id")
        db_.id_ty = confs[1];
    else if (confs[0] == "default_str")
        db_.str_ty = confs[1];
    else if (confs[0] == "default_int")
        db_.int_ty = confs[1];
}

void TypeVisitor::Visit(const SymClause& r)
{
    for (auto& sym : r.symbol())
        db_.symbols[sym->value().lit()] = sym->name();
}

void TypeVisitor::Visit(const Rule& r)
{
    std::string name = r.name();
    Type t(name);

    db_.types.push_back(t);

    Visitor::Visit(r);
}

void TypeVisitor::Visit(const Alternatives& r)
{
    if (r.seqs().size() > 1)
        db_.types.back().is_polymorphic(true);

    int base = db_.types.size() - 1;

    Visitor::Visit(r);

    for (unsigned int i = base + 1; i < db_.types.size(); ++i)
        db_.types[i].base_class(db_.types[base].name());
}

void TypeVisitor::Visit(const Sequence& s)
{
    std::string ty = s.type();
    if (ty != "")
    {
        Type t(ty);
        t.is_polymorphic(true);
        db_.types.push_back(t);
    }

    Visitor::Visit(s);
}

void TypeVisitor::Visit(const NamedId& nid)
{
    db_.types.back().AddMember(nid.name(), nid.id());

    // UGLY
    if (nid.id() == db_.id_ty || nid.id() == db_.str_ty || nid.id() == db_.int_ty)
        db_.types.back().LastAsPrimitive();
    last_was_member_ = true;
    Visitor::Visit(nid);
}

void TypeVisitor::Visit(const Terminal& /*t*/)
{
    last_was_member_ = false;
}

void TypeVisitor::Visit(const NonTerminal& /*t*/)
{
    last_was_member_ = false;
}

void TypeVisitor::Visit(const RuleWhile& t)
{
    if (last_was_member_)
        db_.types.back().LastAsArray();
    Visitor::Visit(t);
}

const TypesDatabase& TypeVisitor::result() const
{
    return db_;
}
