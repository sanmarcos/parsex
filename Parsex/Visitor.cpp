#include "Visitor.h"

void Visitor::Visit(const File& f)
{
    f.tokens()->Accept(*this);
    f.grammar()->Accept(*this);
}

void Visitor::Visit(const TokConf& /*ts*/)
{
}

void Visitor::Visit(const TokSection& ts)
{
    for (auto& conf : ts.general_conf())
        conf->Accept(*this);
    ts.symbols()->Accept(*this);
}

void Visitor::Visit(const SymClause& sc)
{
    for (auto& s : sc.symbol())
        s->Accept(*this);
}

void Visitor::Visit(const Symbol& /*s*/)
{
}

void Visitor::Visit(const RuleGrammar& g)
{
    for (auto& r : g.rules())
        r->Accept(*this);
}

void Visitor::Visit(const Rule& r)
{
    r.alts()->Accept(*this);
}

void Visitor::Visit(const Alternatives& alts)
{
    for (auto& s : alts.seqs())
        s->Accept(*this);
}

void Visitor::Visit(const Sequence& s)
{
    for (auto& e : s.exps())
        e->Accept(*this);
}

void Visitor::Visit(const RuleExp& re)
{
    re.Accept(*this);
}

void Visitor::Visit(const RuleWhile& rw)
{
    rw.exp()->Accept(*this);
}

void Visitor::Visit(const RuleOption& ro)
{
    ro.exp()->Accept(*this);
}

void Visitor::Visit(const RuleId& ri)
{
    ri.id()->Accept(*this);
}

void Visitor::Visit(const Id& ri)
{
    ri.Accept(*this);
}

void Visitor::Visit(const NonTerminal& /*nt*/)
{
}

void Visitor::Visit(const Terminal& /*t*/)
{
}

void Visitor::Visit(const NamedId& /*ni*/)
{
}
