#pragma once

#include <vector>
#include <utility>
#include <string>

struct Parameter
{
    std::string type;
    std::string name;
    bool is_array;
    bool is_primitive;

    Parameter() : is_array(false), is_primitive(false) {}

    std::string type_param() const
    {
        if (is_primitive)
            return type;
        return type + "*";
    }

    std::string type_member() const
    {
        if (is_array)
        {
            if (is_primitive)
                return "std::vector<" + type + ">";
            else
                return "std::vector<std::unique_ptr<" + type + ">>";
        }
        if (is_primitive)
            return type;
        return "std::unique_ptr<" + type + ">";
    }

    std::string type_ret() const
    {
        if (is_array)
        {
            if (is_primitive)
                return "std::vector<" + type + ">&";
            else
                return "std::vector<std::unique_ptr<" + type + ">>&";
        }
        if (is_primitive)
            return type;
        return type + "*";
    }
};

class Type
{
public:
    Type(const std::string& ty);

    void AddMember(const std::string& nm, const std::string& ty);
    void is_polymorphic(bool poly);
    bool is_polymorphic() const;
    void base_class(const std::string& str);
    std::string base_class() const;
    const std::vector<Parameter>& members() const;
    std::string name() const;
    void LastAsArray();
    void LastAsPrimitive();

private:
    std::string ty_;
    std::vector<Parameter> members_;
    std::string base_class_;
    bool is_polymorphic_;
};
