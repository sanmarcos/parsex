#include "FirstVisitor.h"

FirstVisitor::FirstVisitor(const RuleGrammar& g)
    : grammar_(g),
    optional_(false)
{
}

void FirstVisitor::Visit(const Sequence& s)
{
    int i = 0;

    do
    {
        s.exps()[i]->Accept(*this);
        ++i;
    } while (optional_ && i < s.exps().size());
}

void FirstVisitor::Visit(const NonTerminal& nt)
{
    optional_ = false;
    for (auto& r : grammar_.rules())
        if (r->name() == nt.id())
        {
            Visitor::Visit(*r.get());
            return;
        }
    res_.insert(nt.id());
}

void FirstVisitor::Visit(const Terminal& t)
{
    optional_ = false;
    res_.insert(t.id().lit());
}

void FirstVisitor::Visit(const NamedId& ni)
{
    for (auto& r : grammar_.rules())
        if (r->name() == ni.id())
        {
            Visitor::Visit(*r.get());
            return;
        }
    res_.insert(ni.id());
}

void FirstVisitor::Visit(const RuleOption& t)
{
    Visitor::Visit(t);
    optional_ = true;
}

void FirstVisitor::Visit(const RuleWhile& t)
{
    Visitor::Visit(t);
    optional_ = true;
}

const std::set<std::string>& FirstVisitor::res() const
{
    return res_;
}
